ASPEFM=./aspefm
aspefm="clingo clingoLP.py solveLP.lp4" 
clingoLPParams=(-c nstrict=0 -c epsilon="(1,3)" -c accuracy=10)
clingoLPParams="${clingoLPParams[@]}"
minSupport="--heuristic Domain --enum-mode domRec"
clingoParams="--stats=2"
efmChecker="extensions/efm_checker.py"

Collagen=./
Reduced=./reduced/

# Biomass, Collagen, Stroma
echo ':- not support("rsub_8"). :- not support("rsub_4"). :- not support("rsub_30").' > $Collagen/target.lp4 
# Amino acids that are always one direction-only
echo ':- support("rsub_29"). :- support("rsub_1_rev").' >> $Collagen/target.lp4
echo ':- support("rsub_3"). :- support("rsub_2"). :- support("rsub_12_rev").' >> $Collagen/target.lp4
echo ':- support("rsub_26_rev").' >> $Collagen/target.lp4 # Proline production
# Glucose, Lactate, Glutamine
echo ':- not support("rsub_10"). :- not support("rsub_11"). :- not support("rsub_17").' >> $Collagen/target.lp4 
# Unused reactions
echo ':- support("rsub_70"). :- support("rsub_70_rev"). :- support("rsub_39_rev"). :- support("rsub_39").' >> $Collagen/target.lp4
echo ':- support("rsub_20"). :- support("rsub_20_rev"). :- support("rsub_19_rev"). :- support("rsub_19").' >> $Collagen/target.lp4 
echo ':- support("rsub_27_rev").' >> $Collagen/target.lp4
# PMF reaction model
echo ':- not support("rsub_24").' >> $Collagen/target.lp4 # PMF 
echo '&sum{flux("rsub_24")} <= "1.0".' >> $Collagen/target.lp4 # PMF
# Yield constraints: more stroma and collagen than biomass
echo '&sum{flux("rsub_30"); "-1"*flux("rsub_4")} > "0.0".' >> $Collagen/target.lp4
echo '&sum{flux("rsub_8"); "-1"*flux("rsub_4")} > "0.0".' >> $Collagen/target.lp4
# Arbitrary bounds
echo '&sum{flux(R)} < "15.0" :- reaction(R).' >> $Collagen/target.lp4
echo '&sum{flux("rsub_11")} >= "0.1".' >> $Collagen/target.lp4 
echo '&sum{flux("rsub_4")} >= "0.01".' >> $Collagen/target.lp4
echo '&sum{flux(R) : reaction(R)} < "500.0".' >> $Collagen/target.lp4 # around the same as sum of fba fluxes
# Maximum 60 reactions
echo ':- 61 { support(R) : reaction(R) }.' >> $Collagen/target.lp4 # max 60 reactions

essential=" 
:- not support(\"rsub_31\").
:- not support(\"rsub_33\").
:- not support(\"rsub_41\").
:- not support(\"rsub_15\").
:- not support(\"rsub_58\").
:- not support(\"rsub_80\").
:- not support(\"rsub_44\").
:- not support(\"rsub_16\").
:- not support(\"rsub_35\").
:- not support(\"rsub_6\").
% :- support(\"rsub_34\").
% :- support(\"rsub_38\").
" # essential reactions and blocked reactions

echo "$essential" >> $Collagen/target.lp4 

cd $ASPEFM/

for i in `seq 71 75`; do # seq 1 75, 5 by 5 to avoid overclock
    $aspefm $efmChecker --verbose=3 ../$Collagen/target.lp4 --time-limit=302400 -c efmcheckfile=\"../$Reduced/C2M2NF_collagen_reduced.efmc\" ../$Reduced/C2M2NF_collagen_reduced.lp4 -n 0 $clingoLPParams $minSupport $clingoParams --configuration=handy > ../output_collagen_efms_${i}.txt & 
done

cd ..
