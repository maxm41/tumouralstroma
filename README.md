# TumouralStroma

Elementary Flux Modes analysis of a metabolic model of cancer with added tumoral stroma formation.

## Requirements :

This repository uses *[aspefm](https://github.com/maxm4/aspefm)* to compute Elementary Flux Modes using Answer Set Programming (ASP).

The module *[mparser](https://github.com/maxm4/mparser)* should be used for converting metabolic networks into ASP rules.

### Computing EFMs:

*aspefm* is required to launch script `efm_collagen.sh` for computing EFMs. Please refer to *aspefm*'s documentation for installing *clingo* and *cplex*.

A sample of EFMs computed using this bash script is providen in `collagen_all_efms.csv`.

### Installing with submodules :

*aspefm* and *mparser* are submodules of this repository.

Please refer to their respective documentation.

The full repository and its submodules can be downloaded/cloned using:

```git clone --recurse-submodules [repository] ```

See more information: [https://git-scm.com/book/en/v2/Git-Tools-Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

### Python code :

The code requires Python version 3.

Requirements for the following notebooks: `C2M2NFS_analyze_efms.ipynb`, `C2M2NFS_test_compressed.ipynb`:

```
cobra==0.26.2
cplex==22.1.1.2
efmtool_link==0.0.4
matplotlib==3.5.2
numpy==1.21.6
pandas==1.3.5
scikit_learn==1.4.2
scipy==1.7.3
seaborn==0.13.2
```

Requirements for the following notebooks: `C2M2NFS_pfba_sampling.ipynb`, `C2M2NFS_escher_best_solution.ipynb`:

```
cobra==0.26.2
cplex==22.1.1.2
Escher==1.7.3
matplotlib==3.8.0
numpy==1.23.5
pandas==1.5.3
scikit_learn==1.3.1
scipy==1.13.0
seaborn==0.13.2
```

## References :

*C2M2N* was published in the following paper: [Mazat, J.-P. & Ransac, S. The fate of glutamine in human metabolism. The interplay with glucose in proliferating cells. Metabolites 9, 81 (2019).](https://doi.org/10.3390/metabo9050081)

*C2M2NF* was published in the following paper: [Mazat, J.-P. One-carbon metabolism in cancer cells: A critical review based on a core model of central metabolism. Biochemical Society Transactions 49, 1–15 (2021).](https://doi.org/10.1042/BST20190008)

*aspefm* was published in the following paper: [Mahout, M., Carlson, R. P. and Peres, S. Answer Set Programming for Computing Constraints-Based Elementary Flux Modes: Application to Escherichia coli Core Metabolism. Processes 8, 1649 (2020)](https://doi.org/10.3390/pr8121649).

## Contact :

Please contact Maxime Mahout if you have any questions about the repository's content.